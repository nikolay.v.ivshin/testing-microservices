package ru.nivshin.springboot.examples.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.nivshin.springboot.examples.persist.audit.AuditingDateTimeProvider;
import ru.nivshin.springboot.examples.persist.audit.CurrentTimeDateTimeService;
import ru.nivshin.springboot.examples.persist.audit.DateTimeService;
import ru.nivshin.springboot.examples.persist.entities.StoredEvent;
import ru.nivshin.springboot.examples.persist.repositories.StoredEventRepository;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Configuration
@EnableConfigurationProperties
@EnableTransactionManagement
@EnableJpaAuditing(dateTimeProviderRef = "dateTimeProvider")
@EntityScan(basePackageClasses = StoredEvent.class)
@EnableJpaRepositories(basePackageClasses = StoredEventRepository.class)
public class JpaConfig {

    @Bean
    DateTimeProvider dateTimeProvider(DateTimeService dateTimeService) {
        return new AuditingDateTimeProvider(dateTimeService);
    }

    @Bean
    DateTimeService dateTimeService() {
        return new CurrentTimeDateTimeService();
    }
}
