package ru.nivshin.springboot.examples.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.nivshin.springboot.examples.messaging.EventChannels;
import ru.nivshin.springboot.examples.messaging.MessageHandler;
import ru.nivshin.springboot.examples.services.StoredEventErrService;
import ru.nivshin.springboot.examples.services.StoredEventService;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Configuration
@EnableBinding(EventChannels.class)
public class EventsConfig {

    @Bean
    MessageHandler messageHandler(StoredEventService storedEventService, StoredEventErrService storedEventErrService) {
        return new MessageHandler(storedEventService, storedEventErrService);
    }
}
