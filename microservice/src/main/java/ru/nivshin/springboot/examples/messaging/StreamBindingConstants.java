package ru.nivshin.springboot.examples.messaging;

import lombok.experimental.UtilityClass;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@UtilityClass
public class StreamBindingConstants {

    public static final String INCOMING_EVENTS_ERR = "incomingEventsErr";

    public static final String INCOMING_EVENTS = "incomingEvents";

    public static final String OUTGOING_EVENTS = "outgoingEvents";
}
