package ru.nivshin.springboot.examples.persist.audit;

import org.springframework.data.auditing.DateTimeProvider;

import java.time.OffsetDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

public class AuditingDateTimeProvider implements DateTimeProvider {

    private final DateTimeService dateTimeService;

    public AuditingDateTimeProvider(DateTimeService dateTimeService) {
        this.dateTimeService = dateTimeService;
    }

    @Override
    public Optional<TemporalAccessor> getNow() {
        OffsetDateTime dateTime = dateTimeService.getCurrentDateAndTime();
        return dateTime == null ? Optional.empty() : Optional.of(dateTime);
    }
}
