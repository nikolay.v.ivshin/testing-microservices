package ru.nivshin.springboot.examples.messaging;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import ru.nivshin.springboot.examples.dto.EventDto;
import ru.nivshin.springboot.examples.dto.EventErrDto;
import ru.nivshin.springboot.examples.services.StoredEventErrService;
import ru.nivshin.springboot.examples.services.StoredEventService;

import javax.validation.Valid;

import java.io.UnsupportedEncodingException;

import static ru.nivshin.springboot.examples.messaging.StreamBindingConstants.*;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Slf4j
@AllArgsConstructor
public class MessageHandler {

    private final StoredEventService storedEventService;

    private final StoredEventErrService storedEventErrService;

    private static final String UTF_8 = "UTF-8";

    @StreamListener(INCOMING_EVENTS)
    public void handleNormalMessage(@Payload @Valid EventDto eventDto) {
        storedEventService.save(eventDto);
    }

    @StreamListener(INCOMING_EVENTS_ERR)
    public void handleErrMessage(@Payload String failed,
                                 @Header("x-original-topic") byte[] originalTopic,
                                 @Header("x-exception-message") byte[] exceptionMessage,
                                 @Header("x-exception-stacktrace") byte[] exceptionStackTrace) {
        if (StringUtils.isNotBlank(failed)) {
            if (log.isInfoEnabled()) {
                log.info("Handling broken payload: " + failed);
            }
            EventErrDto dto = null;
            try {
                dto = EventErrDto.builder()
                        .content(failed)
                        .originalTopic(new String(originalTopic, UTF_8))
                        .exceptionMessage(new String(exceptionMessage, UTF_8))
                        .exceptionStackTrace(new String(exceptionStackTrace, UTF_8))
                        .build();
            } catch (UnsupportedEncodingException e) {
                log.error("Cannot handle event", e);
                return;
            }
            storedEventErrService.save(dto);
        } else {
            log.error("A message payload is null!");
        }
    }
}
