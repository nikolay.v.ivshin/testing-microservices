package ru.nivshin.springboot.examples.persist.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Entity
@Table(name = "stored_event_err")
@Data
@EqualsAndHashCode(of = "content")
@EntityListeners(AuditingEntityListener.class)
public class StoredEventErr {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
            name = "sequence-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "stored_event_err_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    private Long id;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(length = 4000)
    private String content;

    @Column(name = "original_topic")
    private String originalTopic;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "exception_message", length = 4000)
    private String exceptionMessage;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "exception_stack_trace", length = 4000)
    private String exceptionStackTrace;

    @Setter(value = AccessLevel.PACKAGE)
    @CreatedDate
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime createTime;
}
