package ru.nivshin.springboot.examples;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static java.lang.String.format;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws UnknownHostException {
        Environment env = SpringApplication.run(Application.class, args).getEnvironment();
        String serverPort = env.getProperty("server.port");
        String contextPathEnv = env.getProperty("server.servlet.context-path");
        String contextPath = StringUtils.isBlank(contextPathEnv) ? "" : contextPathEnv;
        String logInfo = format("\n----------------------------------------------------------\n\t"
                        + "Application '%s' is running! Access URLs:\n\t"
                        + "Actuator:\thttp://127.0.0.1:%s%s/actuator/info\n\t"
                        + "Swagger:\thttp://127.0.0.1:%s%s/swagger-ui/\n\t"
                        + "Local: \t\thttp://127.0.0.1:%s%s\n\t"
                        + "External: \thttp://%s:%s%s\n\t"
                        + "Kafka host and port: %s"
                        + "\n----------------------------------------------------------%n",
                env.getProperty("spring.application.name", ""),
                serverPort,
                contextPath,
                serverPort,
                contextPath,
                serverPort,
                contextPath,
                InetAddress.getLocalHost().getHostAddress(),
                serverPort,
                contextPath,
                env.getProperty("spring.cloud.stream.kafka.binder.brokers"));
        log.info(logInfo);
    }
}

