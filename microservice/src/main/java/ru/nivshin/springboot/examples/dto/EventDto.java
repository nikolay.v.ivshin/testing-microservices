package ru.nivshin.springboot.examples.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode(of = "name")
public class EventDto {

    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String value;
}
