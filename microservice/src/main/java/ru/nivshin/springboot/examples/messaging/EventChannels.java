package ru.nivshin.springboot.examples.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

import static ru.nivshin.springboot.examples.messaging.StreamBindingConstants.*;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

public interface EventChannels {

    @Input(INCOMING_EVENTS)
    SubscribableChannel incomingEventChannel();

    @Input(INCOMING_EVENTS_ERR)
    SubscribableChannel incomingEventErrChannel();

    @Output(OUTGOING_EVENTS)
    MessageChannel outgoingEventChannel();
}
