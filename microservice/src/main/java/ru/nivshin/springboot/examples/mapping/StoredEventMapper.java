package ru.nivshin.springboot.examples.mapping;

import org.mapstruct.Mapper;
import ru.nivshin.springboot.examples.dto.EventDto;
import ru.nivshin.springboot.examples.dto.EventErrDto;
import ru.nivshin.springboot.examples.persist.entities.StoredEvent;
import ru.nivshin.springboot.examples.persist.entities.StoredEventErr;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Mapper(componentModel = "spring")
public interface StoredEventMapper {

    EventDto entityToDto(StoredEvent entity);

    StoredEvent dtoToEntity(EventDto dto);

    StoredEventErr eventErrDtoToEntity(EventErrDto dto);

    EventErrDto entityToEventErrDto(StoredEventErr entity);
}
