package ru.nivshin.springboot.examples.services;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nivshin.springboot.examples.dto.EventDto;
import ru.nivshin.springboot.examples.mapping.StoredEventMapper;
import ru.nivshin.springboot.examples.persist.entities.StoredEvent;
import ru.nivshin.springboot.examples.persist.repositories.StoredEventRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class StoredEventService {

    private final StoredEventMapper storedEventMapper;

    private final StoredEventRepository storedEventRepository;

    @Transactional(readOnly = false)
    public EventDto save(EventDto dto) {

        StoredEvent storedEvent = storedEventMapper.dtoToEntity(dto);
        StoredEvent storedEventSaved = storedEventRepository.save(storedEvent);
        return storedEventMapper.entityToDto(storedEventSaved);
    }

    public Page<EventDto> findAll(Pageable pageable) {
        Page<StoredEvent> storedEventsPage = storedEventRepository.findAll(pageable);
        List<EventDto> list =
                storedEventsPage.map(e -> storedEventMapper.entityToDto(e)).stream().collect(Collectors.toList());
        return new PageImpl<>(list, pageable, storedEventsPage.getTotalElements());
    }

    public Optional<EventDto> findByName(String name) {

        Optional<StoredEvent> storedEventOptional = storedEventRepository.findOneByName(name);
        if (storedEventOptional.isPresent()) {
            return storedEventOptional.map(e -> storedEventMapper.entityToDto(e));
        }
        return Optional.empty();
    }
}
