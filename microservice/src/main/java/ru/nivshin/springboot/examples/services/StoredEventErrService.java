package ru.nivshin.springboot.examples.services;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nivshin.springboot.examples.dto.EventErrDto;
import ru.nivshin.springboot.examples.mapping.StoredEventMapper;
import ru.nivshin.springboot.examples.persist.entities.StoredEventErr;
import ru.nivshin.springboot.examples.persist.repositories.StoredEventErrRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class StoredEventErrService {

    private final StoredEventMapper storedEventMapper;

    private final StoredEventErrRepository storedEventErrRepository;

    @Transactional(readOnly = false)
    public EventErrDto save(EventErrDto dto) {

        StoredEventErr errEntity = storedEventMapper.eventErrDtoToEntity(dto);
        StoredEventErr saved = storedEventErrRepository.save(errEntity);
        return storedEventMapper.entityToEventErrDto(saved);
    }

    public Page<EventErrDto> findAll(Pageable pageable) {
        Page<StoredEventErr> page = storedEventErrRepository.findAll(pageable);
        List<EventErrDto> list = page.map(e -> storedEventMapper.entityToEventErrDto(e)).stream()
                        .collect(Collectors.toList());
        return new PageImpl<>(list, pageable, page.getTotalElements());
    }

    public Optional<EventErrDto> findByContent(String content) {
        Optional<StoredEventErr> storedEventOptional = storedEventErrRepository.findOneByContent(content);
        if (storedEventOptional.isPresent()) {
            return storedEventOptional.map(e -> storedEventMapper.entityToEventErrDto(e));
        }
        return Optional.empty();
    }
}
