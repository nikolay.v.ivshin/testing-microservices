package ru.nivshin.springboot.examples.controllers;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.nivshin.springboot.examples.dto.EventDto;
import ru.nivshin.springboot.examples.dto.EventErrDto;
import ru.nivshin.springboot.examples.services.StoredEventErrService;
import ru.nivshin.springboot.examples.services.StoredEventService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.Optional;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Slf4j
@RestController
@AllArgsConstructor
public class StoredEventController {

    private final StoredEventService storedEventService;

    private final StoredEventErrService storedEventErrService;

    @ApiOperation(value = "A method to save a StoredEvent object",
            produces = "application/json",
            response = EventDto.class)
    @RequestMapping(path = "/stored_event", method = {RequestMethod.POST, RequestMethod.PUT})
    public EventDto save(@RequestBody @Valid EventDto dto) {
        return storedEventService.save(dto);
    }

    @GetMapping(path = "/stored_events")
    @ApiOperation(value = "A method to find all normal messages in paged way",
            produces = "application/json",
            response = EventDto.class, responseContainer = "List")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "Integer", paramType = "query", value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "Integer", paramType = "query", value = "Number of records per page.", defaultValue = "20"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc)")
    })
    public Page<EventDto> find(
            @ApiIgnore("Ignored because swagger ui shows the wrong params, instead they are explained in the implicit params")
                    Pageable pageable) {
        return storedEventService.findAll(pageable);
    }

    @GetMapping(path = "/stored_event")
    @ApiOperation(value = "A method to find a messages by name attribute",
            produces = "application/json",
            response = EventDto.class)
    public EventDto findByName(
            @ApiParam(required = true, value = "Parameter to filter persisted messages by name attribute")
            @RequestParam(name = "name") String name) {
        Optional<EventDto> result = storedEventService.findByName(name);
        if (result.isPresent()) {
            return result.get();
        }
        throw new ResponseStatusException(NOT_FOUND, format("No messages with name attribute '%s' were found", name));
    }

    @GetMapping(path = "/stored_dlq_events")
    @ApiOperation(value = "A method to find all DLQ-messages in paged way",
            produces = "application/json",
            response = EventErrDto.class, responseContainer = "List")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "Integer", paramType = "query", value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "Integer", paramType = "query", value = "Number of records per page.", defaultValue = "20"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc)")
    })
    public Page<EventErrDto> findErrorMessages(
            @ApiIgnore("Ignored because swagger ui shows the wrong params, instead they are explained in the implicit params")
                    Pageable pageable) {
        log.info("Requesting DLQ data records: page = {}, page size = {} ",
                pageable.getPageNumber(), pageable.getPageSize());
        return storedEventErrService.findAll(pageable);
    }

    @GetMapping(path = "/stored_dlq_event")
    @ApiOperation(value = "A method to find a DLQ messages by name attribute",
            produces = "application/json",
            response = EventErrDto.class)
    public EventErrDto findDlqByName(
            @ApiParam(required = true, value = "Parameter to filter persisted DLQ messages by content attribute")
            @RequestParam(name = "content") String content) {
        if (log.isInfoEnabled()) {
            log.info("Requesting DLQ data by message content: " + content);
        }
        Optional<EventErrDto> result = storedEventErrService.findByContent(content);
        if (result.isPresent()) {
            return result.get();
        }
        throw new ResponseStatusException(NOT_FOUND, format("No messages with content attribute '%s' were found", content));
    }
}
