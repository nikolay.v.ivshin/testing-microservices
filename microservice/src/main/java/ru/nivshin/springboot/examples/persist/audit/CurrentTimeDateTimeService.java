package ru.nivshin.springboot.examples.persist.audit;

import java.time.OffsetDateTime;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

public class CurrentTimeDateTimeService implements DateTimeService {

    @Override
    public OffsetDateTime getCurrentDateAndTime() {
        return OffsetDateTime.now();
    }
}
