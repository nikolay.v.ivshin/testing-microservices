package ru.nivshin.springboot.examples.persist.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Entity
@Table(name = "stored_event")
@Data
@EqualsAndHashCode(of = "name")
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
public class StoredEvent {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
            name = "sequence-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "stored_event_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String value;

    @Setter(value = AccessLevel.PACKAGE)
    @CreatedDate
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime createTime;

    @Setter(value = AccessLevel.PACKAGE)
    @LastModifiedDate
    @Column(name = "change_time", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime changeTime;

    public StoredEvent(Long id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }
}
