package ru.nivshin.springboot.examples.messaging;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.MimeTypeUtils;
import ru.nivshin.springboot.examples.dto.EventDto;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Slf4j
@AllArgsConstructor
public class MessageSender {

    private final EventChannels eventChannels;

    public void sendEventMessage(EventDto eventDto) {
        eventChannels.outgoingEventChannel().send(MessageBuilder
                .withPayload(eventDto)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }

    public void sendEventMessageAsString(String content) {
        eventChannels.outgoingEventChannel().send(MessageBuilder
                .withPayload(content)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }
}
