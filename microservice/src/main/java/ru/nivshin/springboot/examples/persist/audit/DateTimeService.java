package ru.nivshin.springboot.examples.persist.audit;

import java.time.OffsetDateTime;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

public interface DateTimeService {

    OffsetDateTime getCurrentDateAndTime();
}
