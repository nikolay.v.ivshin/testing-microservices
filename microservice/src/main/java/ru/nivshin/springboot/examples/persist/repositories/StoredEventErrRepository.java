package ru.nivshin.springboot.examples.persist.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nivshin.springboot.examples.persist.entities.StoredEventErr;

import java.util.Optional;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Repository
public interface StoredEventErrRepository extends JpaRepository<StoredEventErr, Long> {

    Optional<StoredEventErr> findOneByContent(String content);
}
