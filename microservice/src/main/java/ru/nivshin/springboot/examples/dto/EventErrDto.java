package ru.nivshin.springboot.examples.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Data
@EqualsAndHashCode(of = "content")
public class EventErrDto {

    @NotBlank
    private String content;

    private String originalTopic;

    private String exceptionMessage;

    private String exceptionStackTrace;
}
