package ru.nivshin.springboot.examples.integrationtests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.cloud.stream.test.binder.TestSupportBinderAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.nivshin.springboot.examples.dto.EventDto;
import ru.nivshin.springboot.examples.mapping.StoredEventMapperImpl;
import ru.nivshin.springboot.examples.messaging.MessageSender;
import ru.nivshin.springboot.examples.metaannotations.IntegrationTest;
import ru.nivshin.springboot.examples.services.StoredEventService;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.concurrent.TimeUnit.*;
import static org.awaitility.Awaitility.await;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static ru.nivshin.springboot.examples.config.Constants.CHECK_FOR_NOT_NULL_ONLY;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Slf4j
@RunWith(SpringRunner.class)
@IntegrationTest()
//@EnableAutoConfiguration(exclude = TestSupportBinderAutoConfiguration.class)
@Import({StoredEventService.class, StoredEventMapperImpl.class})
public class EventsTest {

    @Autowired
    private MessageSender messageSender;

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @ClassRule // No JUnit5 support yet
    public static EmbeddedKafkaRule embeddedKafka =
            new EmbeddedKafkaRule(1, true, "incoming-events-demo");

    @BeforeClass
    public static void setUp() {
        Awaitility.setDefaultPollInterval(500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(Duration.ZERO);
        Awaitility.setDefaultTimeout(10, SECONDS);
    }

    private EventDto buildTestMessagePayloadObject(String suffix) {
        return EventDto.builder().name("test name " + suffix).value("test value " + suffix).build();
    }

    @SneakyThrows
    private String buildTestMessagePayloadString(Object dto) {
        return objectMapper.writeValueAsString(dto);
    }

    @SneakyThrows
    private boolean hasPersistedData(String url,
                                     MultiValueMap<String, String> filterParams,
                                     Map<String, String> expectedValues) {
        ResultActions resultActions = mockMvc.perform(get(url)
                .params(filterParams)
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print());

        int actualStatus = resultActions.andReturn().getResponse().getStatus();
        if (actualStatus != OK.value()) {
            return false;
        }
        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        JsonNode rootNode = objectMapper.readTree(responseJson);
        for (Map.Entry<String, String> entry : expectedValues.entrySet()) {
            JsonNode valueNode = rootNode.findValue(entry.getKey());
            if (valueNode == null) {
                log.error("Missing attribute: " + entry.getKey());
                return false;
            }
            boolean checkForNullOnly = CHECK_FOR_NOT_NULL_ONLY.equals(entry.getValue());
            if (!checkForNullOnly) {
                boolean isEqual = expectedValues.get(entry.getKey()).equals(valueNode.asText());
                if (!isEqual) {
                    log.error("Incorrect attribute value: " + entry.getKey());
                    return false;
                }
            }
        }
        return true;
    }

    @Test
    public void testNormalEvent() {
        String suffix = "1";
        EventDto dto = buildTestMessagePayloadObject(suffix);
        messageSender.sendEventMessage(dto);
        await().until(() -> {
            Map<String, String> expectedAttributes = new HashMap<>();
            expectedAttributes.put("value", dto.getValue());
            MultiValueMap<String, String> filterParams = new LinkedMultiValueMap<>();
            filterParams.put("name", Collections.singletonList(dto.getName()));
            return hasPersistedData("/stored_event", filterParams, expectedAttributes);
        });
    }

    @Test
    public void testBrokenJsonEvent() {
        String suffix = "2";
        EventDto dto = buildTestMessagePayloadObject(suffix);
        String incorrectJson = buildTestMessagePayloadString(dto).replaceAll(",", "");
        messageSender.sendEventMessageAsString(incorrectJson);
        await().until(() -> {
            Map<String, String> expectedAttributes= new HashMap<>();
            expectedAttributes.put("content", incorrectJson);
            expectedAttributes.put("originalTopic", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionMessage", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionStackTrace", CHECK_FOR_NOT_NULL_ONLY);
            MultiValueMap<String, String> filterParams = new LinkedMultiValueMap<>();
            filterParams.put("content", Collections.singletonList(incorrectJson));
            return hasPersistedData("/stored_dlq_event", filterParams, expectedAttributes);
        });
    }

    @Test
    public void testMissingRequiredValueEvent() {
        String suffix = "3";
        EventDto dto = buildTestMessagePayloadObject(suffix);
        dto.setValue(null); // Setting Null for required attribute
        String incorrectJson = buildTestMessagePayloadString(dto);
        messageSender.sendEventMessage(dto);
        await().until(() -> {
            Map<String, String> expectedAttributes= new HashMap<>();
            expectedAttributes.put("content", incorrectJson);
            expectedAttributes.put("originalTopic", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionMessage", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionStackTrace", CHECK_FOR_NOT_NULL_ONLY);
            MultiValueMap<String, String> filterParams = new LinkedMultiValueMap<>();
            filterParams.put("content", Collections.singletonList(incorrectJson));
            return hasPersistedData("/stored_dlq_event", filterParams, expectedAttributes);
        });
    }
}
