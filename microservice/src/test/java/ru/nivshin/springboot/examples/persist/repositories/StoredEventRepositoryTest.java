package ru.nivshin.springboot.examples.persist.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nivshin.springboot.examples.metaannotations.RepositoryTest;
import ru.nivshin.springboot.examples.persist.entities.StoredEvent;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@RunWith(SpringRunner.class)
@RepositoryTest
@Transactional
public class StoredEventRepositoryTest {

    @Autowired
    private StoredEventRepository storedEventRepository;

    @Test
    public void testStoredEventRepository() {

        StoredEvent storedEvent = storedEventRepository.save(new StoredEvent(null, "name", "value"));
        assertThat(storedEvent.getId()).isNotNull();
    }
}
