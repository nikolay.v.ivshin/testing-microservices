package ru.nivshin.springboot.examples.config;

import io.zonky.test.db.postgres.embedded.PgBinaryResolver;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

import static java.lang.String.format;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

public class CustomPostgresBinaryResolver implements PgBinaryResolver {
    public InputStream getPgBinary(String system, String architecture) throws IOException {
        ClassPathResource resource = new ClassPathResource(format("postgres-%s-%s.txz", system, architecture)
                .toLowerCase());
        return resource.getInputStream();
    }
}
