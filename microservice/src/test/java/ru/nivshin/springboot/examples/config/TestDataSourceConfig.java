package ru.nivshin.springboot.examples.config;

import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import io.zonky.test.db.postgres.embedded.PgBinaryResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Slf4j
@Configuration
public class TestDataSourceConfig {

    @Bean
    PgBinaryResolver pgBinaryResolver() {
        return new CustomPostgresBinaryResolver();
    }

    @Bean(name = "postgresProcess", destroyMethod = "close")
    EmbeddedPostgres postgresProcess(PgBinaryResolver pgBinaryResolver) throws IOException {

        try (EmbeddedPostgres pg = EmbeddedPostgres.builder()
                .setPgBinaryResolver(pgBinaryResolver)
                .start()) {
            return pg;
        } catch (Exception ex) {
            log.error("Cannot start postgres process");
            throw ex;
        }
    }

    @SuppressWarnings("ContextJavaBeanUnresolvedMethodsInspection")
    @Bean
    @Primary
    @DependsOn({"postgresProcess"})
    DataSource dataSource(EmbeddedPostgres embeddedPostgres) {
        return embeddedPostgres.getPostgresDatabase();
    }
}
