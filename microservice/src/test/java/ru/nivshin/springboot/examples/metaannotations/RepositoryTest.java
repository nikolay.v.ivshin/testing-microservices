package ru.nivshin.springboot.examples.metaannotations;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.core.annotation.AliasFor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import ru.nivshin.springboot.examples.config.JpaConfig;
import ru.nivshin.springboot.examples.config.TestDataSourceConfig;

import java.lang.annotation.*;

/**
 * A meta-annotation fo testing Spring Data layer
 * @see DataJpaTest for alternative implementation
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@DirtiesContext
@ContextConfiguration(
        classes = {TestDataSourceConfig.class, JpaConfig.class},
        initializers = ConfigDataApplicationContextInitializer.class)
@AutoConfigureEmbeddedDatabase
@AutoConfigureDataJpa
@AutoConfigureTestEntityManager
@ImportAutoConfiguration
@ActiveProfiles
public @interface RepositoryTest {

    @AliasFor(annotation = ActiveProfiles.class, attribute = "profiles") String[] activeProfiles() default {"test"};
}
