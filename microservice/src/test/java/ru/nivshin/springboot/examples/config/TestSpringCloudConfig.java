package ru.nivshin.springboot.examples.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.nivshin.springboot.examples.messaging.EventChannels;
import ru.nivshin.springboot.examples.messaging.MessageSender;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Configuration
public class TestSpringCloudConfig {

    @Bean
    public MessageSender messageSender(EventChannels eventChannels) {
        return new MessageSender(eventChannels);
    }
}
