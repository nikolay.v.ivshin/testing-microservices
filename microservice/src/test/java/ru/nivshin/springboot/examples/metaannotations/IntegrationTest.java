package ru.nivshin.springboot.examples.metaannotations;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import ru.nivshin.springboot.examples.config.TestDataSourceConfig;
import ru.nivshin.springboot.examples.config.TestSpringCloudConfig;

import java.lang.annotation.*;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@DirtiesContext
@SpringBootTest
@AutoConfigureMockMvc
@Import({TestSpringCloudConfig.class, TestDataSourceConfig.class})
@AutoConfigureEmbeddedDatabase
@ActiveProfiles
public @interface IntegrationTest {

    @AliasFor(annotation = ActiveProfiles.class, attribute = "profiles") String[] activeProfiles() default {"test"};

    /**
     * @see WebMvcTest#value
     */
    @AliasFor(annotation = SpringBootTest.class, attribute = "webEnvironment")
    SpringBootTest.WebEnvironment webEnvironment() default SpringBootTest.WebEnvironment.RANDOM_PORT;

}
