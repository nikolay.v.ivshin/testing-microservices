package ru.nivshin.springboot.examples.config;

import lombok.experimental.UtilityClass;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */

@UtilityClass
public class Constants {

    public static final String CHECK_FOR_NOT_NULL_ONLY = "|???|";
}
