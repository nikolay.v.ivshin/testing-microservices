Spring cloud streams
https://www.baeldung.com/spring-cloud-stream
https://cloud.spring.io/spring-cloud-static/spring-cloud-stream-binder-kafka/2.2.0.RC1/spring-cloud-stream-binder-kafka.html
https://spring.io/blog/2017/10/24/how-to-test-spring-cloud-stream-applications-part-i

Awaitility
https://www.baeldung.com/awaitlity-testing

Testcontainers
https://www.testcontainers.org/modules/docker_compose/

Check host mapping. The following lines must be present there for host resolution from local network:
127.0.0.1    kafka
127.0.0.1    postgres
