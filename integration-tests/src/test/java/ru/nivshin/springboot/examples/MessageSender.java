package ru.nivshin.springboot.examples;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Slf4j
public class MessageSender {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    private final String host;

    private final String port;

    public MessageSender(String environmentConfigFile) {

        this.host = "localhost";
        this.port = ResourceUtil.getEnvironmentVariable(environmentConfigFile, "KAFKA_PORT");
        this.kafkaTemplate = buildKafkaTemplate();
    }

    void send(String topic, String payload) {
        log.info("Sending payload='{}' to the topic='{}'", payload, topic);
        kafkaTemplate.send(topic, payload);
    }

    @SneakyThrows
    private KafkaTemplate<String, Object> buildKafkaTemplate() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host + ":" + port);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        ProducerFactory<String, Object> producerFactory = new DefaultKafkaProducerFactory<>(configProps);
        return new KafkaTemplate<>(producerFactory);
    }
}
