package ru.nivshin.springboot.examples;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.*;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.parallel.ResourceAccessMode.READ;
import static org.springframework.http.HttpStatus.OK;
import static org.testcontainers.shaded.org.apache.commons.lang.StringUtils.isBlank;
import static ru.nivshin.springboot.examples.Constants.CHECK_FOR_NOT_NULL_ONLY;

/**
 * Интеграционный тест с Testcontainers: Postgres
 * https://www.testcontainers.org/modules/docker_compose/
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Slf4j
@Testcontainers
@ActiveProfiles("test")
class PostgresIntegrationTest {

    private static final ContainerBuilder containerBuilder = ContainerBuilder.getInstance();

    private ObjectMapper objectMapper = new ObjectMapper();

    private static final String DOCKER_COMPOSE = "docker-compose";

    private static final String TOPIC_ENV_VARIABLE = "EVENT_TOPIC";

    @Container
    public DockerComposeContainer<?> environment =
            containerBuilder.buildContainer(
                    "src/test/resources/compose/docker-compose-with-application.yml",
                    "compose/docker-env/app-dev.env");

    private MessageSender messageSender = new MessageSender(containerBuilder.getEnvironmentConfigFile());

    @BeforeAll
    public static void beforeAll() {
        Awaitility.setDefaultPollInterval(1000, MILLISECONDS);
        Awaitility.setDefaultPollDelay(Duration.ZERO);
        Awaitility.setDefaultTimeout(10, SECONDS);
    }

    private Map<String, String> buildTestMessagePayloadObject(String suffix) {
        Map<String, String> object = new HashMap<>();
        object.put("name", "test name " + suffix);
        object.put("value", "test value " + suffix);
        return object;
    }

    @SneakyThrows
    private String buildTestMessagePayloadString(Object dto) {
        return objectMapper.writeValueAsString(dto);
    }

    @SneakyThrows
    private boolean hasPersistedData(String url,
                                     MultiValueMap<String, String> filterParams,
                                     Map<String, String> expectedValues) {

        TypeReference<Map<String, String>> typeReference = new TypeReference<Map<String, String>>() {};
        Map<String, String> response = RequestUtil.doGetRequest(url, filterParams, typeReference, objectMapper, OK);
        if (response == null) {
            return false;
        }
        String responseJson = objectMapper.writeValueAsString(response);
        JsonNode rootNode = objectMapper.readTree(responseJson);
        for (Map.Entry<String, String> entry : expectedValues.entrySet()) {
            JsonNode valueNode = rootNode.findValue(entry.getKey());
            if (valueNode == null) {
                log.error("Missing attribute: " + entry.getKey());
                return false;
            }
            boolean checkForNullOnly = CHECK_FOR_NOT_NULL_ONLY.equals(entry.getValue());
            if (!checkForNullOnly) {
                boolean isEqual = expectedValues.get(entry.getKey()).equals(valueNode.asText());
                if (!isEqual) {
                    log.error("Incorrect attribute value: " + entry.getKey());
                    return false;
                }
            }
        }
        return true;
    }

    private String getContextPath() {
        String port = ResourceUtil.getEnvironmentVariable(
                containerBuilder.getEnvironmentConfigFile(), "SERVICE_PORT");
        String context = ResourceUtil.getEnvironmentVariable(
                containerBuilder.getEnvironmentConfigFile(), "SERVICE_NAME");
        context = isBlank(context) ? "" : "/" + context;
        return format("http://localhost:%s%s", port, context);
    }

    @Test
    @ResourceLock(value = DOCKER_COMPOSE, mode = READ)
    @SneakyThrows
    void testNormalEvent() {
        String suffix = "1";
        Map<String, String> dto = buildTestMessagePayloadObject(suffix);
        String topic = ResourceUtil.getEnvironmentVariable(containerBuilder.getEnvironmentConfigFile(), TOPIC_ENV_VARIABLE);
        messageSender.send(topic, objectMapper.writeValueAsString(dto));
        final String context = getContextPath();
        await().until(() -> {
            Map<String, String> expectedAttributes = new HashMap<>();
            expectedAttributes.put("value", dto.get("value"));
            MultiValueMap<String, String> filterParams = new LinkedMultiValueMap<>();
            filterParams.put("name", Collections.singletonList(dto.get("name")));
            return hasPersistedData(context + "/stored_event", filterParams, expectedAttributes);
        });
    }

    @Test
    @ResourceLock(value = DOCKER_COMPOSE, mode = READ)
    void testBrokenJsonEvent() {
        String suffix = "2";
        Map<String, String> dto = buildTestMessagePayloadObject(suffix);
        String incorrectJson = buildTestMessagePayloadString(dto).replaceAll(",", "");
        String topic = ResourceUtil.getEnvironmentVariable(containerBuilder.getEnvironmentConfigFile(), TOPIC_ENV_VARIABLE);
        messageSender.send(topic, incorrectJson);
        final String context = getContextPath();
        await().until(() -> {
            Map<String, String> expectedAttributes= new HashMap<>();
            expectedAttributes.put("content", incorrectJson);
            expectedAttributes.put("originalTopic", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionMessage", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionStackTrace", CHECK_FOR_NOT_NULL_ONLY);
            MultiValueMap<String, String> filterParams = new LinkedMultiValueMap<>();
            filterParams.put("content", Collections.singletonList(incorrectJson));
            return hasPersistedData(context + "/stored_dlq_event", filterParams, expectedAttributes);
        });
    }

    @Test
    @ResourceLock(value = DOCKER_COMPOSE, mode = READ)
    @SneakyThrows
    void testMissingRequiredValueEvent() {
        String suffix = "3";
        Map<String, String> dto = buildTestMessagePayloadObject(suffix);
        dto.put("value", null); // Setting Null for required attribute
        String incorrectJson = buildTestMessagePayloadString(dto);
        String topic = ResourceUtil.getEnvironmentVariable(containerBuilder.getEnvironmentConfigFile(), TOPIC_ENV_VARIABLE);
        messageSender.send(topic, objectMapper.writeValueAsString(dto));
        final String context = getContextPath();
        await().until(() -> {
            Map<String, String> expectedAttributes= new HashMap<>();
            expectedAttributes.put("content", incorrectJson);
            expectedAttributes.put("originalTopic", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionMessage", CHECK_FOR_NOT_NULL_ONLY);
            expectedAttributes.put("exceptionStackTrace", CHECK_FOR_NOT_NULL_ONLY);
            MultiValueMap<String, String> filterParams = new LinkedMultiValueMap<>();
            filterParams.put("content", Collections.singletonList(incorrectJson));
            return hasPersistedData(context + "/stored_dlq_event", filterParams, expectedAttributes);
        });
    }
}
