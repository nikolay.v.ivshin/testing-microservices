package ru.nivshin.springboot.examples;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.cdimascio.dotenv.Dotenv;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Slf4j
@UtilityClass
public class ResourceUtil {

    public static class RestResponsePage<T> extends PageImpl<T> {

        private static final long serialVersionUID = 3248189030448292002L;

        @JsonCreator(mode = PROPERTIES)
        public RestResponsePage(@JsonProperty("content") List<T> content,
                                @JsonProperty("number") int number,
                                @JsonProperty("size") int size,
                                @JsonProperty("totalElements") Long totalElements,
                                @JsonProperty("pageable") JsonNode pageable,
                                @JsonProperty("last") boolean last,
                                @JsonProperty("totalPages") int totalPages,
                                @JsonProperty("sort") JsonNode sort,
                                @JsonProperty("first") boolean first,
                                @JsonProperty("numberOfElements") int numberOfElements) {
            super(content, PageRequest.of(number, size), totalElements);
        }

        public RestResponsePage(List<T> content, Pageable pageable, long total) {
            super(content, pageable, total);
        }

        public RestResponsePage(List<T> content) {
            super(content);
        }

        public RestResponsePage() {
            super(new ArrayList<>());
        }

    }

    @SneakyThrows
    public String getEnvironmentVariable(String resource, String variableName) {

        URL url = Thread.currentThread().getContextClassLoader().getResource(resource);
        if (url == null) {
            throw new IllegalArgumentException("Missing resource: " + resource);
        }
        Path filePath = Paths.get(url.toURI().getPath());
        Dotenv env = Dotenv.configure()
                .directory(filePath.getParent().toString())
                .filename(filePath.getFileName().toString()).load();
        return env.get(variableName);
    }

    @SneakyThrows
    public <T> T retrieveResourceFromResponse(HttpResponse response, TypeReference<T> typeRefer, ObjectMapper mapper) {
        String jsonFromResponse = EntityUtils.toString(response.getEntity());
        return mapper.readValue(jsonFromResponse, typeRefer);
    }

    @SneakyThrows
    public String readResource(String resource) {

        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource)) {
            if (inputStream == null) {
                throw new RuntimeException("Missing resource: " + resource);
            }
            StringWriter writer = new StringWriter();
            String encoding = UTF_8.name();
            IOUtils.copy(inputStream, writer, encoding);
            return writer.toString();
        }
    }
}
