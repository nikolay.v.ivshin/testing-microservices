package ru.nivshin.springboot.examples;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;
import java.time.Duration;

import static org.apache.http.util.TextUtils.isBlank;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Slf4j
public class ContainerBuilder {

    @Getter
    private String environmentConfigFile;

    private static class InstanceKeeper {
        private static final ContainerBuilder instance = new ContainerBuilder();
    }

    public static ContainerBuilder getInstance() {
        return InstanceKeeper.instance;
    }

    //================================================================================================

    DockerComposeContainer<?> buildContainer(
            String dockerComposeConfig, String environmentConfigFile) {

        this.environmentConfigFile = environmentConfigFile;
        int timeoutInMinutes = 5;

        String servletContext = ResourceUtil.getEnvironmentVariable(environmentConfigFile, "SERVICE_NAME");
        servletContext = isBlank(servletContext) ? "" : ("/" + servletContext);

        return new DockerComposeContainer<>(new File(dockerComposeConfig))
                .waitingFor("zookeeper", new HostPortWaitStrategy())
                .waitingFor("kafka", new HostPortWaitStrategy())
                .waitingFor("postgres", new HostPortWaitStrategy())
                .waitingFor("app", Wait.forHttp(servletContext + "/swagger-ui/")
                        .forStatusCodeMatching(it -> it >= 200 && it < 300)
                        .withStartupTimeout(Duration.ofMinutes(timeoutInMinutes)));
    }
}

