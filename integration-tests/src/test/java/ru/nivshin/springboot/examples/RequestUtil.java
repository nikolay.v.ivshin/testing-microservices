package ru.nivshin.springboot.examples;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Slf4j
@UtilityClass
public class RequestUtil {

    @SneakyThrows
    public <T> T doGetRequest(String url,
                              MultiValueMap<String, String> filterParams,
                              TypeReference<T> typeReference,
                              ObjectMapper mapper,
                              HttpStatus expectedStatus) {

        URIBuilder uriBuilder = new URIBuilder(url);
        for (Map.Entry<String, List<String>> entry : filterParams.entrySet()) {
            for (String value : entry.getValue()) {
                uriBuilder.addParameter(entry.getKey(), value);
            }
        }
        URI requestUrl = uriBuilder.build();
        log.info("Requesting GET: " + requestUrl);
        HttpUriRequest request = new HttpGet(requestUrl);

        List<Header> headers = new ArrayList<>();
        headers.add(new BasicHeader("Cache-Control", "no-store, max-age=0"));
        HttpResponse response = HttpClientBuilder.create()
                .setDefaultHeaders(headers)
                .build().execute(request);
        boolean isMatchingStatus = response.getStatusLine().getStatusCode() == expectedStatus.value();
        if (!isMatchingStatus) {
            log.info("HTTP status core {} doesn't match expected value {}",
                    response.getStatusLine().getStatusCode(), expectedStatus.value());
            return null;
        }
        T object = ResourceUtil.retrieveResourceFromResponse(response, typeReference, mapper);
        log.info("Available models: " + object);
        return object;
    }

}
