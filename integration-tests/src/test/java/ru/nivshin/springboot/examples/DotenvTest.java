package ru.nivshin.springboot.examples;

import io.github.cdimascio.dotenv.Dotenv;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author Nikolay Ivshin (nikolay.v.ivshin@gmail.com)
 */
@Slf4j
public class DotenvTest {

    @Test
    public void testDotenv() throws URISyntaxException {

        Path filePath =
                Paths.get(getClass().getResource("/compose/docker-env/app-dev.env").toURI().getPath());

        Dotenv env = Dotenv.configure()
                .directory(filePath.getParent().toString())
                .filename(filePath.getFileName().toString()).load();
        assertThat(env.get("SERVICE_PORT")).isEqualTo("8080");
    }
}
